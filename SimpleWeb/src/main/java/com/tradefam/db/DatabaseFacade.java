package com.tradefam.db;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseFacade {
	public static List<String> getBooks() {       
	//  Database credentials
	      String DB_URL = "jdbc:postgresql://127.0.0.1:5432/postgres";
	      String USER = "postgres";
	      String PASS = "0986";
		
		List<String> result = new ArrayList<String>();
//		try {
//			Class.forName("org.postgresql.Driver");
//		} catch (ClassNotFoundException e) {
//			System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
//			e.printStackTrace();
//		}
		
    try (
    	Connection connection = DriverManager.getConnection(DB_URL,USER,PASS)) { 
    	System.out.println("Java JDBC PostgreSQL Example");
        System.out.println("Connected to PostgreSQL database!");
        Statement statement = connection.createStatement();            
        ResultSet resultSet = statement.executeQuery("SELECT * FROM books.books");
        int columns = resultSet.getMetaData().getColumnCount();
        while (resultSet.next()) {
            result.add(resultSet.getString("name"));
            }        
        System.out.println(resultSet);
        System.out.println(result);
        } catch (SQLException e) {
        System.out.println("Connection failure.");
        e.printStackTrace();
    }
    return result;    
    }
	
}
